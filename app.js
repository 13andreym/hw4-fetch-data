function fetchData(url) {
    return new Promise(function(resolve, reject) {
      const xhr = new XMLHttpRequest();
      xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            const data = JSON.parse(xhr.responseText);
            resolve(data);
          } else {
            reject(xhr.statusText);
          }
        }
      };
      xhr.open('GET', url, true);
      xhr.send();
    });
  }

  fetchData('https://ajax.test-danit.com/api/swapi/films')
    .then(function(filmsData) {
      const filmsContainer = document.getElementById('films-container');

      filmsData.forEach(function(film) {
        const filmContainer = document.createElement('div');
        filmContainer.classList.add('film');

        filmContainer.innerHTML += `<h2>Episode ${film.episodeId}: ${film.name}</h2>`;
        filmContainer.innerHTML += `<p>${film.openingCrawl}</p>`;

        const charactersPromises = film.characters.map(function(characterUrl) {
          return fetchData(characterUrl);
        });

        Promise.all(charactersPromises)
          .then(function(charactersData) {
            filmContainer.innerHTML += '<h3>Characters:</h3>';
            charactersData.forEach(function(characterData) {
              filmContainer.innerHTML += `<p>${characterData.name}</p>`;
            });


            filmsContainer.appendChild(filmContainer);
          });
      });
    })
    .catch(function(error) {
      console.error(error);
    });